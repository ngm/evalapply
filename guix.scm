(use-modules (guix profiles)
             (gnu packages base)
             (gnu packages guile))

(packages->manifest
 (list glibc
       glibc-utf8-locales
       gnu-make
       guile-2.2
       guile-syntax-highlight
       haunt))
