;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt html)
             (haunt page)
             (haunt post)
             (haunt reader)
             (haunt reader commonmark)
             (haunt site)
             (haunt utils)
             (commonmark)
	     ;; (syntax-highlight)
	     ;; (syntax-highlight scheme)
             ;; (syntax-highlight xml)
             ;; (syntax-highlight c)
             (sxml match)
             (sxml transform)
             (texinfo)
             (texinfo html)
             (srfi srfi-1)
             (srfi srfi-19)
             (ice-9 rdelim)
             (ice-9 regex)
             (ice-9 match)
             (web uri))

(define (date year month day)
  "Create a SRFI-19 date for the given YEAR, MONTH, DAY"
  (let ((tzoffset (tm:gmtoff (localtime (time-second (current-time))))))
    (make-date 0 0 0 0 day month year tzoffset)))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "/css/" name ".css")))))

(define* (anchor content #:optional (uri content))
  `(a (@ (href ,uri)) ,content))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "Creative Commons Attribution Share-Alike 4.0 International"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-sa/4.0/80x15.png")))))

(define (link name uri)
  `(a (@ (href ,uri)) ,name))

(define* (centered-image url #:optional alt)
  `(img (@ (class "centered-image")
           (src ,url)
           ,@(if alt
                 `((alt ,alt))
                 '()))))

(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define evalapply-theme
  (theme #:name "evalapply"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (title ,(string-append title " — " (site-title site)))
              ,(stylesheet "reset")
              ,(stylesheet "fonts")
              ,(stylesheet "evalapply"))
             (body
              (div (@ (class "container"))
                   (div (@ (class "nav"))
                        (ul (li ,(link "EvalApply" "/"))
                            (li (@ (class "fade-text")) " ")
                            (li ,(link "SICP" "/sicp.html"))
                            (li ,(link "Resources" "/resources.html"))
                            (li ,(link "Come along" "/come-along.html"))
                            (li ,(link "Code of conduct" "/conduct.html"))))
                   ,body
                   (footer (@ (class "text-center"))
                           (p (@ (class "copyright"))
                              "© 2019 EvalApply"
                              ,%cc-by-sa-button)
                           (p "The text and images on this site are
free culture works available under the " ,%cc-by-sa-link " license.")
                           (p "This website is built with "
                              (a (@ (href "http://haunt.dthompson.us"))
                                 "Haunt")
                              ", a static site generator written in "
                              (a (@ (href "https://gnu.org/software/guile"))
                                 "Guile Scheme")
                              "."))))))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))

           `((h1 ,title)
             ,(map (lambda (post)
                     (let ((uri (string-append "/"
                                               (site-post-slug site post)
                                               ".html")))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(define %collections
  `(("Recent Blog Posts" "index.html" ,posts/reverse-chronological)))

(define parse-lang
  (let ((rx (make-regexp "-*-[ ]+([a-z]*)[ ]+-*-")))
    (lambda (port)
      (let ((line (read-line port)))
        (match:substring (regexp-exec rx line) 1)))))

(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                 ('scheme lex-scheme)
                 ('xml    lex-xml)
                 ('c      lex-c)
                 (_ #f))))
    (if lexer
        (highlights->sxml (highlight lexer source))
        source)))

(define (sxml-identity . args) args)

(define (highlight-code . tree)
  (sxml-match tree
    ((code (@ (class ,class) . ,attrs) ,source)
     (let ((lang (string->symbol
                  (string-drop class (string-length "language-")))))
       `(code (@ ,@attrs)
             ,(maybe-highlight-code lang source))))
    (,other other)))

(define (highlight-scheme code)
  `(pre (code ,(highlights->sxml (highlight lex-scheme code)))))

(define (raw-snippet code)
  `(pre (code ,(if (string? code) code (read-string code)))))

;; Markdown doesn't support video, so let's hack around that!  Find
;; <img> tags with a ".webm" source and substitute a <video> tag.
(define (media-hackery . tree)
  (sxml-match tree
    ((img (@ (src ,src) . ,attrs) . ,body)
     (if (string-suffix? ".webm" src)
         `(video (@ (src ,src) (controls "true"),@attrs) ,@body)
         tree))))

(define %commonmark-rules
  `((code . ,highlight-code)
    (img . ,media-hackery)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (values (read-metadata-headers port)
                             (post-process-commonmark
                              (commonmark->sxml port))))))))

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout evalapply-theme site title body)
               sxml->html)))

(define intro-page
  (static-page
   "Hello"
   "index.html"
   `((h2 "Hello and welcome to EvalApply!")
     (p "We are a collective of people learning to program in Lisp.")
     (p ,(centered-image "/images/evalapply.jpeg" "Eval Apply"))
     (p "We meet regularly to learn, teach, and chat with one another.  We are self-educating in the fundamental and philosophical principles of computer programming through the study of the classic textbook " ,(link "Structure and Interpretation of Computer Programs" "/sicp.html") "."))))

(define sicp-page
  (static-page
   "Structure and Interpretation of Computer Programs"
   "sicp.html"
   `((h2 "Structure and Interpretation of Computer Programs")
     (p ,(centered-image "/images/sicp.jpeg" "SICP front cover")))))



(define code-of-conduct-page
  (static-page
   "Code of conduct"
   "conduct.html"
   `((h2 "Code of conduct")
     (p "Welcome to EvalApply, a space for anyone interested in learning to program.")
     (p "Anyone is welcome to join, whether you’re experienced in programming or just starting out and wanting to learn. When getting involved in our open space, we ask everyone to respect our community values by following the code of conduct below.")

     (h3 "We are social")
     (h3 "We are radically open and actively inclusive")

     (p "We wish to be a diverse bunch from all walks of life and with different life experiences. We welcome all people willing to be radically open: of all appearances, genders, sexualities, nationalities, abilities, backgrounds and political leanings.")
     (p "We take time to get to know each other and consider how others may think and feel, and how comments or actions may be perceived in a diverse community. We all make mistakes or assumptions about others, no matter how open we think we are, but please consider the potential impact of your actions on the people around you. When people express discomfort or raise an issue, listen carefully first before reacting.")
     (p "Even if you disagree with something, take the lead from professional critics: they foster healthy conversation and debate without ever attacking people personally. Think about that when responding to others, and ask yourself the question: would I say it to their face?")

     (h3 "We are friendly")
     (p "It's really important that we make people who come to our space feel welcome straight away so that we don’t loss them before they’ve gotten started. You can put people at easy in really simple ways: acknowledging their presence (saying hi), letting them know how it works (what do they need to do next, what can they expect), offering them a ‘way-in’ (a cup of tea, a place to wait, a way to contribute).")

     (h3 "We are hands-on")
     (p "...")

     (h3 "We do-it-together")
     (p "...")

     (h3 "We have fun")
     (p "...")

     (h3 "We are system-changing")
     (p "..."))))

(define resources-page
  (static-page
   "Resources"
   "resources.html"
   `((h2 "Resources")
     (h3 "Videos")
     (h3 "Book")
     (h3 "Answers")
     (h3 "Tools")
     )))

(define come-along-page
  (static-page
   "Come along"
   "come-along.html"
   `((h2 "Come along")
     (p "We meet fortnightly at " ,(link  "The Mayday Rooms" "https://www.maydayrooms.org/") " from 18:00 till 20:00.")
     (h3 "Address")
     (p "The Mayday Rooms, 88 Fleet Street, London, EC4Y 1DH.")
     (p "Unfortunately the Mayday Rooms is up several flights of stairs and there is no lift, so it is not wheelchair accessible.")
     (h3 "Upcoming sessions")
     (p "Our sessions go from 18:00 till 20:00.")
     (p "Tuesday 16 July 2019.")
     (p "Tuesday 30 July 2019.")
     (p "Tuesday 13 August 2019.")
     (p "Tuesday 27 August 2019.")
     (p "Tuesday 10 September 2019.")
     (p "Tuesday 24 September 2019.")
     )))

(site #:title "evalapply"
      #:domain "evalapply.space"
      #:default-metadata
      '((author . "Eval Apply")
        (email  . "lambda@gizmonaut.net"))
      #:readers (list commonmark-reader*)
      #:builders (list intro-page
                       sicp-page
                       code-of-conduct-page
                       resources-page
                       come-along-page
                       ;(blog #:theme evalapply-theme #:collections %collections)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       ;(static-directory "js")
                       (static-directory "css")
                       (static-directory "fonts")
                       (static-directory "images")))
